from django.db import models

# Create your models here.
# model = database 
# the class name = database name
# any variables is a field 
# any variable parameter is field type
class Post(models.Model):
    text = models.TextField()

    def __str__(self):
        return self.text[:50]
